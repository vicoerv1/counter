data "aws_caller_identity" "current" {}

variable "aws_access_key" {
  type = string
}

variable "aws_secret_key" {
  type = string
}

variable "bastion_key_name" {
  type = string
}
