provider "aws" {
  region     = "ap-southeast-1"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

data "aws_availability_zones" "available" {
  state = "available"

  filter {
    name   = "region-name"
    values = ["ap-southeast-1"]
  }
}

data "aws_vpc" "default" {
  default = true
}

data "aws_internet_gateway" "gw" {
  filter {
    name = "attachment.vpc-id"
    values = ["${data.aws_vpc.default.id}"]
  }
}

resource "aws_default_subnet" "public_a" {
  availability_zone = "ap-southeast-1a"
}

resource "aws_default_subnet" "public_b" {
  availability_zone = "ap-southeast-1b"
}

resource "aws_subnet" "private" {
  vpc_id                  = data.aws_vpc.default.id
  cidr_block              = "172.31.48.0/20"
  availability_zone       = data.aws_availability_zones.available.names[0]
  map_public_ip_on_launch = false
 
  tags = {
    Name = "counter-private-subnet"
  }
}

resource "aws_eip" "eip" {
  vpc = true
}

resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.eip.id
  subnet_id = aws_default_subnet.public_a.id

  tags = {
    Name = "Counter NAT"
  }
}

resource "aws_route_table" "NAT_route_table" {
  depends_on = [
    data.aws_vpc.default,
    aws_nat_gateway.nat,
  ]

  vpc_id = data.aws_vpc.default.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat.id
  }

  tags = {
    Name = "NAT-route-table"
  }
}

resource "aws_route_table_association" "associate_routetable_to_private_subnet" {
  depends_on = [
    aws_subnet.private,
    aws_route_table.NAT_route_table,
  ]
  subnet_id      = aws_subnet.private.id
  route_table_id = aws_route_table.NAT_route_table.id
}

# bastion
resource "aws_security_group" "sg_bastion_host" {
  depends_on = [
    data.aws_vpc.default,
  ]
  name        = "sg bastion host"
  description = "bastion host security group"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "aws_ami" "amazon-linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-*-hvm-*-x86_64-gp2"]
  }
}

# bastion host ec2 instance
resource "aws_instance" "bastion_host" {
  depends_on = [
    aws_security_group.sg_bastion_host,
  ]
  ami = data.aws_ami.amazon-linux.id
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.sg_bastion_host.id]
  subnet_id = aws_default_subnet.public_a.id
  key_name = var.bastion_key_name
  tags = {
      Name = "bastion host"
  }
}
# end bastion

resource "aws_iam_policy" "counter_s3_policy" {
  name        = "counter_s3_policy"
  path        = "/"
  description = "Policy to provide permission to instance"
  
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "ssm:GetParameters",
          "ssm:GetParameter"
        ],
        Resource = "arn:aws:ssm:ap-southeast-1:${data.aws_caller_identity.current.account_id}:parameter/dev*"
      },
      {
        Effect = "Allow",
        Action = [
          "s3:GetObject",
          "s3:List*"
        ],
        Resource = [
          "arn:aws:s3:::counter-1105/web/*"
        ]
      }
    ]
  })
}

resource "aws_iam_role" "instance_role" {
  name = "instance_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_policy_attachment" "instance_policy_role" {
  name       = "instance_attachment"
  roles      = [aws_iam_role.instance_role.name]
  policy_arn = aws_iam_policy.counter_s3_policy.arn
}

resource "aws_iam_instance_profile" "instance_profile" {
  name = "instance_profile"
  role = aws_iam_role.instance_role.name
}

resource "aws_s3_bucket" "counter-bucket" {
  bucket = "counter-1105"
}

resource "aws_s3_object" "object" {
  bucket = "counter-1105"
  key    = "web/index.html"
  source = "./web/index.html"

  depends_on = [aws_s3_bucket.counter-bucket]
}

resource "aws_launch_configuration" "counter" {
  name_prefix          = "counter-"
  image_id             = data.aws_ami.amazon-linux.id
  instance_type        = "t2.micro"
  user_data            = file("user-data.sh")
  security_groups      = [aws_security_group.counter_instance.id]
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "counter" {
  name                 = "counter"
  min_size             = 1
  max_size             = 3
  desired_capacity     = 1
  launch_configuration = aws_launch_configuration.counter.name
  vpc_zone_identifier  = [aws_subnet.private.id]

  tag {
    key                 = "Name"
    value               = "VicoErv - Counter"
    propagate_at_launch = true
  }
}

resource "aws_lb" "counter" {
  name               = "counter-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.counter_lb.id]
  subnets            = [aws_default_subnet.public_a.id, aws_default_subnet.public_b.id]
}

resource "aws_lb_listener" "counter" {
  load_balancer_arn = aws_lb.counter.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.counter.arn
  }
}

resource "aws_lb_target_group" "counter" {
  name     = "counter"
  port     = 80
  protocol = "HTTP"
  vpc_id   = data.aws_vpc.default.id
}


resource "aws_autoscaling_attachment" "counter" {
  autoscaling_group_name = aws_autoscaling_group.counter.id
  lb_target_group_arn   = aws_lb_target_group.counter.arn
}

resource "aws_security_group" "counter_instance" {
  name = "counter-instance"
  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.counter_lb.id]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "counter_lb" {
  name = "counter-lb"
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

