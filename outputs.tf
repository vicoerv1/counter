output "lb_endpoint" {
  value = "http://${aws_lb.counter.dns_name}"
}

output "application_endpoint" {
  value = "http://${aws_lb.counter.dns_name}/index.html"
}

output "asg_name" {
  value = aws_autoscaling_group.counter.name
}

